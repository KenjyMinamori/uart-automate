/*
 * settings.c
 *
 *  Created on: 4 ���. 2016 �.
 *      Author: Mikhail
 */
#include "settings.h"

GPIO_InitTypeDef    GPIO_InitStruct;
USART_InitTypeDef    USART_InitStruct;


void settings(void){
	UART_init();
	//adc_settings();
	//dac_settings();
	InitPeriph();
	leds_init();
	buttons_init();
}


void UART_init(void){
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);

	GPIO_InitStruct.GPIO_Pin |= GPIO_Pin_9 | GPIO_Pin_10;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;

	GPIO_Init(GPIOA, &GPIO_InitStruct);

	USART_InitStruct.USART_BaudRate = 9600;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_Parity = USART_Parity_No ;
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_Init(USART1, &USART_InitStruct);

	USART_Cmd(USART1, ENABLE);
}

void adc_settings(void)
{
	gpio_init();
	RCC->CR |= RCC_CR_HSION; 			//�������� ���������� ��������� HSI - 16���
	while(!(RCC->CR&RCC_CR_HSIRDY)); 	//���� ��� ������������
	RCC->APB2ENR |= RCC_APB2ENR_ADC1EN; //��������� ������������ ���
	ADC1->CR2 |= ADC_CR2_ADON; 			//�������� ���
	while(!(ADC1->SR&ADC_SR_ADONS)); 	//���� ���������� ���
	ADC1->SQR5 |= (ADC_SQR5_SQ1_2 | ADC_SQR5_SQ1_0); //������������� ���� 0 � 2. ����� �������, �������� ����� ADC_IN5 ��� ���������� 1-�� ��������������
	ADC1->CR1 &= ~ADC_CR1_RES; 			//����������� ��� - 12 ���
	ADC1->CR2 &= ~ADC_CR2_ALIGN; 		//������������ ���������� ������
}


void dac_settings(void){
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6,ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);


	TIM6->PSC = 0;
	TIM6->ARR = 500;
	TIM6->DIER |= TIM_DIER_UIE;
	TIM6->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM6_IRQn);


	DAC->CR |= DAC_CR_EN1;
}


void gpio_init(void)
{
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN; //��������� ������������ ����� �
	GPIOA->MODER |= GPIO_MODER_MODER5;  //������������� 11 - ���������� ����/����� ��� PA5
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR5; //������� ���� 00 - ���������� ����/����� ��� �������� ��� PA5

}


void InitPeriph(void)
{
  //��������� ����� �
  RCC->AHBENR |= RCC_AHBENR_GPIOAEN; //�������� ������������ ����� �
  GPIOA->MODER |= GPIO_MODER_MODER5; //���������� ����/����� ����� PA5
  GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR5; //��������� ������������� ���������

  //��������� �����-����������� ���������������
  RCC->APB1ENR |= RCC_APB1ENR_DACEN; //�������� ������������ DAC
  DAC->CR |= DAC_CR_EN2; //�������� ����� 2 DAC
  DAC->CR |= DAC_CR_TEN2; //��������� ������ �������������� � ������ 2 DAC
  DAC->CR |= DAC_CR_TSEL2; //�������� �������� ������� - �����������
}


void leds_init(void){

	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);


}

void greenOn(void){
	GPIOB->ODR |= GPIO_ODR_ODR_7;
}

void greenOff(void){
	GPIOB->ODR &= ~GPIO_ODR_ODR_7;
}


void blueOn(void){
	GPIO_SetBits(GPIOB, GPIO_Pin_6);
}

void blueOff(void){
	GPIO_ResetBits(GPIOB, GPIO_Pin_6);
}

void buttons_init(void){
	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

}

uint8_t user_button(void){
	return GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_1);
}
