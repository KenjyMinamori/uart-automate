#include <stddef.h>
#include "main.h"
#include "ringbuf.h"

#define RX_BUFFER_SIZE 2048
#define BUF_LEN 250



uint8_t RXLineBuff[RX_BUFFER_SIZE];
uint8_t TXLineBuff[RX_BUFFER_SIZE];

ringBuf RXRingBuff;
ringBuf TXRingBuff;

void buffers_init(void){
	ringBufferInit(&RXRingBuff, RXLineBuff, sizeof(RXLineBuff));
	ringBufferInit(&TXRingBuff, TXLineBuff, sizeof(TXLineBuff));
}

uint8_t uart_data;
char buffer[BUF_LEN];
uint8_t buf_i = 0;
uint16_t dacOut = 1500;

automate status = wait_command;



int main(void)
{
	settings();
	buffers_init();
	int i = 0;
	while (1)
	{
		if (ringBufferGet(&RXRingBuff, &uart_data)) {
			handle_letter(uart_data);
		}
		// TODO: FIX THIS UGLY SHIT
		for (int li = 0; li < 30; li++){
			USART1_send();
		}
		DAC->DHR12R2 = dacOut;
		DAC->SWTRIGR |= DAC_SWTRIGR_SWTRIG2;

		if (user_button()){
			blueOn();
			send_str("\r\nUSER");
		}

		i++;
	}
	return 0;
}


void send_byte(uint8_t data) {
	ringBufferPut(&TXRingBuff, data);
}
void USART1_IRQHandler(void) {
	if (USART1->SR & USART_SR_RXNE) {
		rx_byte = USART1->DR;
		ringBufferPut(&RXRingBuff, rx_byte);
	}
}

void USART1_send(void){
	uint8_t data;
	if (ringBufferGet(&TXRingBuff, &data)) {
		while (!(USART1->SR & USART_SR_TC))
			;
		USART1->DR = data;
	}
}


void send_str(char * string) {
	uint8_t i = 0;
	send_byte('\r');
	send_byte('\n');
	while (string[i]) {
		send_byte(string[i]);
		i++;
	}
	send_byte('\r');
	send_byte('\n');
}

int handle_letter(char letter){
	// ����� ������� ������
	if (letter == '\r' || letter == '\n'){
		handle_line(buf_i);
		clear_buffer();
		send_str("");
	} else {
		USART1_Send(uart_data);


		// Overflow?
		if (buf_i >= BUF_LEN){
			clear_buffer();
			send_str("[w] USART1 buffer overflow!");
			return -1;

		// Catch Ctrl + C
		} else if (letter == '\003'){
			status = wait_command;
			greenOff();
			clear_buffer();
			send_str("");
			return -1;

		// catch backspace
		} else if (letter == '\177'){
			if (buf_i > 0){ --buf_i; }
			return 0;

		// OK
		} else {
			buffer[buf_i] = letter;
			++buf_i;
			return 0;

		}
	}
	return 0;

}

int handle_line(uint8_t len) {
	uint8_t digit = 0;
	uint32_t answer = 0;
	uint32_t d = 1;
	buffer[len]='\0';
	switch (status){
	case wait_command:
		greenOff();
		if (strcmp(buffer, "hello") == 0) {
			greenOff();
			send_str("hello, Anton");
		} else if (strcmp(buffer, "set") == 0) {
			send_str("waitng num");
			greenOn();

			status = wait_int;
		} else if (strcmp(buffer, "test") == 0) {
			send_str("Test!.. Test!.. Can you hear me?");
		} else if (strcmp(buffer, "blue_on") == 0) {
			blueOn();
		} else if (strcmp(buffer, "blue_off") == 0) {
			blueOff();
		} else {
			send_str("unknown command");
		}


		break;
	case wait_int:

		if (len > 31){
			send_str("[e] Int is too big");
			return -1;
		}
		for (int i = len - 1; i >= 0; i--){
			digit = buffer[i];

			if (digit >= 0x30 && digit <= 0x39) {
				digit -= 0x30;
				answer += digit * d;
				d *= 10;
			} else if (digit >= 0x41 && digit <= 0x46){
				digit -= 0x41 + 0xA;
				answer += digit * d;
				d *= 10;
			} else {
				send_str("[e] Wrong int");
				return -1;
			}
		}
		if (answer > 3000) {
			send_str("too big!");
			return 0;
		}
		dacOut = (uint16_t)(answer / (float)3000 * 4095);
		return 0;
		break;
	default:
		break;
	}
	return 0;
}

void TIM6_DAC_IRQHandler(void) {
	TIM6->SR &= ~TIM_SR_UIF;
	DAC->DHR12R1=dacOut;

}

void clear_buffer(void){
	buffer[0]='\0';
	buf_i = 0;
}


