/*
 * settings.h
 *
 *  Created on: 4 ���. 2016 �.
 *      Author: Mikhail
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_
#include <stddef.h>
#include "stm32l1xx.h"
#include "stm32l1xx_it.h"
#include "stm32l1xx_rcc.h"
#include "stm32l1xx_gpio.h"
#include "stm32l1xx_usart.h"


// Settings for UART1
void UART_init(void);

// Settings for ADC
void adc_settings(void);

void InitPeriph(void);

//
void gpio_init(void);

// Settings for adc
void dac_settings(void);


void leds_init(void);
void greenOn(void);
void greenOff(void);
void blueOn(void);
void blueOff(void);

void buttons_init(void);
uint8_t user_button(void);


void settings(void);
#endif /* SETTINGS_H_ */
