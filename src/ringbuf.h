/**
 * @file   ringbuf.h
 * @author Roman Savrulin (romeo.deepmind@gmail.com)
 * @date   08.07.2017
 * @brief  ������� ��������� ������
 *
 * ��� ���������� ������ ������� �� ����� � �� ������ �� ������ ���� ����� ��� �� ������ ������
 */

#ifndef __RINGBUF_H
#define __RINGBUF_H

#include "stm32f10x.h"

/**
 * @brief ��������� ���������� �������
 *
 * ringBuf capacity is size-1, so size should be >=2
 */
typedef struct {
	uint8_t  *start;
	uint8_t  *head;
	uint8_t  *tail;
	uint8_t  *end;
}ringBuf;

/**
 * @brief �������������� ��������� ��������� @ref ringBuf
 *
 */
void ringBufferInit(ringBuf* rb, uint8_t* buf, uint32_t size);

/**
 * @brief ������ ������ ����� � ������ ��� ������� � ��� �����
 *
 */
uint32_t ringBufferPut(volatile ringBuf* rb, uint8_t data);

/**
 * @brief ������ ������ ����� �� ������� ��� ������� � ��� ������
 *
 */
uint32_t ringBufferGet(volatile ringBuf* rb, uint8_t *data);

/**
 * @brief ������ ���������� ���������� ������ � ������
 *
 */
uint32_t ringBufferWrite(volatile ringBuf* rb, uint8_t* data, uint32_t size);

/**
 * @brief ������ ���������� ���������� ������ �� �������
 *
 */
uint32_t ringBufferRead(volatile ringBuf* rb, uint8_t* data, uint32_t size);

/**
 * @brief �������� �� ������� ������ � �������
 *
 */
uint32_t ringBufferCheck(volatile ringBuf* rb);

/**
 * @brief ������� �������
 *
 */
void ringBufferReset(volatile ringBuf* rb);

#endif //__RINGBUF_H