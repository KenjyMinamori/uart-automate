#include "stm32l1xx.h"

#include "stm32l1xx_it.h"
#include <string.h>
#include "settings.h"

// Send one word by UART1
void USART1_Send(uint8_t);

// Send string with /r/n by UART1
void send_str(char * string);


// CLears buffer
void clear_buffer(void);

// Handle the RX byte
int handle_letter(char letter);

// Handle the whole line
int handle_line(uint8_t len);




typedef enum
{
    wait_int = 1,
	wait_command = 0
} automate;


