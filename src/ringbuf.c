/**
 * @file   ringbuf.c
 * @author Roman Savrulin (romeo.deepmind@gmail.com)
 * @date   08.07.2017
 * @brief  ������� ��������� ������
 *
 * ��� ���������� ������ ������� �� ����� � �� ������ �� ������ ���� ����� ��� �� ������ ������
 */

#include "ringbuf.h"
#include <string.h>

/**
 * @brief �������������� ��������� ��������� @ref ringBuf
 *
 * @param rb - ��������� �� ��������� @ref ringBuf
 * @param buf - ��������� �� �������� ������ (������� ��� ���������� ������)
 * @param size - ������ ��������� �������
 */
void ringBufferInit(ringBuf* rb, uint8_t* buf, uint32_t size){
	memset(rb, 0, sizeof(ringBuf));
	rb->head = buf;
	rb->tail = buf;
	rb->start = buf;
	rb->end = buf + size;
}

/**
 * @brief ���������� ��������� ������ �� ���������� �������
 *
 * @param rb - ��������� �� ��������� @ref ringBuf
 * @param ptr - ��������� �� ������ ��� ����� ������ (rb->tail ��� rb->head)
 *
 * @retval ����������� ���������
 */
static inline uint8_t *ringBufIncPtr(volatile ringBuf *rb, uint8_t *ptr){
	uint8_t * tmp = ptr + 1;
	if(tmp >= rb->end)
		tmp = rb->start;
	return tmp;
}

/**
 * @brief ������ ������ ����� � ������ ��� ������� � ��� �����
 *
 * @param rb - ��������� �� ��������� @ref ringBuf
 * @param data - ���� ������� ����� ������� � ������
 *
 * @retval 1 - �����, 0 - ������ �� �����������
 */
uint32_t ringBufferPut(volatile ringBuf* rb, uint8_t data){
	uint8_t* newHead = ringBufIncPtr(rb, rb->head);

	if(newHead == rb->tail){ //no space in buffer;
		return 0;
	}

	*rb->head = data;
	rb->head = newHead;

	return 1;
}

/**
 * @brief ������ ������ ����� �� ������� ��� ������� � ��� ������
 *
 * @param rb - ��������� �� ��������� @ref ringBuf
 * @param data - ��������� �� ����������, � ������� ����� ������� ���� ������
 *
 * @retval 1 - �����, 0 - ������ ����
 */
uint32_t ringBufferGet(volatile ringBuf* rb, uint8_t *data){
	if(rb->tail == rb->head) //no data in buffer;
		return 0;

	uint8_t *newTail = ringBufIncPtr(rb, rb->tail);

	*data = *rb->tail;
	rb->tail = newTail;

	return 1;
}

/**
 * @brief ������ ���������� ���������� ������ � ������
 *
 * @param rb - ��������� �� ��������� @ref ringBuf
 * @param data - ��������� �� ������ ������ ��� ������
 * @param size - ���������� ������ ��� ������ (� ������)
 *
 * @retval 4 �����: ���������� ���������� ������
 */
uint32_t ringBufferWrite(volatile ringBuf* rb, uint8_t* data, uint32_t size){
	uint32_t i=0;
	while(i<size&&ringBufferPut(rb, data[i])){
		i++;
	}
	return i;
}

/**
 * @brief ������ ���������� ���������� ������ �� �������
 *
 * @param rb - ��������� �� ��������� @ref ringBuf
 * @param data - ��������� �� ������� ������, ���� ����� �������� ������ �� �������
 * @param size - ���������� ������ ��� ������ (� ������)
 *
 * @retval 4 �����: ���������� ��������� ������
 */
uint32_t ringBufferRead(volatile ringBuf* rb, uint8_t* data, uint32_t size){
	uint32_t i=0;
	while(i<size&&ringBufferGet(rb, &data[i])){
		i++;
	}
	return i;
}

/**
 * @brief �������� �� ������� ������ � �������
 *
 * @param rb - ��������� �� ��������� @ref ringBuf
 *
 * @retval 1 - � ������� ���� ������, 2 - ������ ����
 */
uint32_t ringBufferCheck(volatile ringBuf* rb){
	if(rb->tail == rb->head) //no data in buffer;
		return 0;	
	return 1;
}

/**
 * @brief ������� �������
 *
 * @param rb - ��������� �� ��������� @ref ringBuf
 */
void ringBufferReset(volatile ringBuf* rb){
	//TODO: race protection?
	rb->tail = rb->start;
	rb->head = rb->tail;
}
